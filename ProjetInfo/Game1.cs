﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ProjetInfo.Code;
using ProjetInfo.Code.Managers;
using ProjetInfo.Code.Objects;
using ProjetInfo.Code.World;
using ProjetInfo.Code.World.Levels;

namespace ProjetInfo
{

    public class Game1 : Game
    {
        public static GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private SpriteFont _font;

        public static System.Drawing.Rectangle res = Screen.PrimaryScreen.Bounds;

        public Texture2D White;

        public Camera Camera { get; set; }
        public GameSprite Player { get; set; }

        public List<GameObject> Objects { get; set; }
        public List<GameObject> ObjectsDecor { get; set; }
        public List<GameObject> ObjectsDynamic { get; set; }

        public List<GameObject> ObjectsToAdd { get; set; }
        public List<GameObject> ObjectsToRemove { get; set; }

        public List<BoundingBox> DecorHitbox { get; set; }


        private static Game1 instance;

        public Level CurrentLevel { get; set; }

        public float GameSpeed { get; set; }

        public const float GravityConstant = -650f;

        public Hammer Hammer { get; set; }

        public string content;

        public Game1()
        {
            content = "Content";
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = res.Width;
            graphics.PreferredBackBufferHeight = res.Height;
            graphics.IsFullScreen = true;
            Content.RootDirectory = content;

            GameSpeed = 1f;
            instance = this;
        }

        protected override void Initialize()
        {
            base.Initialize();
            spriteBatch = new SpriteBatch(GraphicsDevice);
            _font = Content.Load<SpriteFont>("test");
            White = Content.Load<Texture2D>("white");

            List<Texture2D> Tes = new List<Texture2D>
            {
                Content.Load<Texture2D>("Textures/Player/PlayerStatic"),
                Content.Load<Texture2D>("Textures/Player/PlayerMoveForward"),
                Content.Load<Texture2D>("Textures/Player/PlayerMoveFace"),
                Content.Load<Texture2D>("Textures/Player/PlayerMoveRight"),
                Content.Load<Texture2D>("Textures/Player/PlayerMoveLeft")
            };
            Player = new GameSprite(Content, Vector3.Zero, false, 2, Tes, true, 1f, 0f);
            Player.isPlayer = true;

            Camera = new Camera(graphics.GraphicsDevice, Player);
            
            Hammer = new Hammer(Content, Player.Position);

            ObjectsToAdd = new List<GameObject>();
            ObjectsToRemove = new List<GameObject>();

            CurrentLevel = new MovementTutorial(this);
            CurrentLevel.Load();

        }

        protected override void LoadContent()
        {
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            ObjectsToAdd.Clear();
            ObjectsToRemove.Clear();
            InputManager.GetInput(this, gameTime);

            foreach (GameObject obj in ObjectsDecor)
            {
                  obj.Update(gameTime, GameSpeed, null);
            }
            List<GameObject> UpObjects = new List<GameObject>() { }; 

            foreach (GameObject obj in ObjectsDynamic)
            {
                foreach(GameObject upObject in Objects)
                {
                    if (upObject != obj) UpObjects.Add(upObject);
                }
                obj.Update(gameTime, GameSpeed, UpObjects);
                UpObjects.Clear();
            }

            ObjectsDynamic.AddRange(ObjectsToAdd);
            Objects.AddRange(ObjectsToAdd);

            foreach (GameObject obj in ObjectsToAdd)
            {
                foreach (GameObject upObject in Objects)
                {
                    if (upObject != obj) UpObjects.Add(upObject);
                }
                obj.Update(gameTime, GameSpeed, UpObjects);
                UpObjects.Clear();
            }

            Camera.Update(gameTime);

            CurrentLevel.UpdateLevel();

            foreach (GameObject obj in ObjectsToRemove)
            {
                ObjectsDynamic.Remove(obj);
                Objects.Remove(obj);
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin(); ;

            if (CurrentLevel.Text != null) DrawText(Color.White, Color.Black, CurrentLevel.Text, new Vector2(5, 5));
            graphics.GraphicsDevice.BlendState = BlendState.AlphaBlend;
            graphics.GraphicsDevice.RasterizerState = RasterizerState.CullNone;
            graphics.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            graphics.GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;


            GraphicsDevice.Clear(Color.CornflowerBlue);

            Hammer.Draw(Camera);

            foreach (GameObject obj in ObjectsDecor)
            {
                obj.Draw(graphics, Camera);
            }


            SortObjectsByDistance(ObjectsDynamic);
            foreach (GameObject obj in ObjectsDynamic)
            {
                obj.Draw(graphics, Camera);
                //obj.DrawHitBox();
            }

            //foreach (BoundingBox box in DecorHitbox)
            //{
            //    Vector3[] CornersList = box.GetCorners();

            //    foreach (Vector3 start in CornersList)
            //    {
            //        foreach (Vector3 end in CornersList)
            //        {
            //            DrawLine(start, end);
            //        }
            //    }
            //}

            //Hammer.DrawHitBox();

            //Player.DrawHitBox();

            base.Draw(gameTime);
            spriteBatch.End();

        }

        public void DrawText(Color colorFont, Color colorBackground, List<string> texts, Vector2 position)
        {
           
            float windowsHeight = graphics.PreferredBackBufferHeight;
            float windowsWidth = graphics.PreferredBackBufferWidth;
            position.Y *= windowsHeight / 100;
            position.X *= windowsWidth / 100;

            float textSizeX = 0;
            float textSizeY = 0;

            foreach (string text in texts)
            {
                if (textSizeX < _font.MeasureString(text).X) textSizeX = _font.MeasureString(text).X;
                if (textSizeY < _font.MeasureString(text).Y) textSizeY = _font.MeasureString(text).Y;
            }

            spriteBatch.Draw(White, new Rectangle((int)position.X - 10, (int)position.Y - 10, (int)textSizeX + 20, texts.Count * ((int)textSizeY + 3) + 17), colorBackground * 0.5f);

            for(int i = 0; i < texts.Count; i++)
            {
                spriteBatch.DrawString(_font, texts[i], position, colorFont);
                position.Y += (int)textSizeY + 3;
            }
        }

        private void SortObjectsByDistance(List<GameObject> Objects)
        {
            for (int i = 0; i < Objects.Count - 1; i++)
            {
                while (CoordinatesManager.Norme(Objects[i].Position - Camera.Position) < CoordinatesManager.Norme(Objects[i + 1].Position - Camera.Position))
                {
                    GameObject tempoGameObject = Objects[i];
                    Objects[i] = Objects[i + 1];
                    Objects[i + 1] = tempoGameObject;

                }
            }
        }

        public static void DrawLine(Vector3 start, Vector3 end)
        {
            instance.PrivateDrawLine(start, end);
        }

        private void PrivateDrawLine(Vector3 start, Vector3 end)
        {
            BasicEffect basicEffect = new BasicEffect(graphics.GraphicsDevice);
            basicEffect.View = Camera.ViewMatrix;
            basicEffect.Projection = Camera.ProjectionMatrix;

            basicEffect.CurrentTechnique.Passes[0].Apply();
            var vertices = new[] { new VertexPositionColor(start, Color.Red), new VertexPositionColor(end, Color.Red) };
            GraphicsDevice.DrawUserPrimitives(PrimitiveType.LineList, vertices, 0, 1);
        }
    }
}
