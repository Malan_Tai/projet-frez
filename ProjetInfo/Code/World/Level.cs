﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using ProjetInfo.Code.Objects;
using Microsoft.Xna.Framework.Graphics;

namespace ProjetInfo.Code.World
{
    public abstract class Level
    {
        protected Game1 gameInstance;
        protected Level nextLevel;
        public Vector3 barycentre;

        public List<string> Text { get; set; }

        protected ContentManager Content 
        {
            get
            {
                return gameInstance.Content;
            }
        }

        public Level(Game1 game)
        {
            gameInstance = game;
        }

        abstract protected void Build();

        abstract public bool UpdateLevel();

        public void Load()
        {
            List<Texture2D> Tes = new List<Texture2D>
            {
                Content.Load<Texture2D>("Textures/Player/PlayerStatic"),
                Content.Load<Texture2D>("Textures/Player/PlayerMoveForward"),
                Content.Load<Texture2D>("Textures/Player/PlayerMoveFace"),
                Content.Load<Texture2D>("Textures/Player/PlayerMoveRight"),
                Content.Load<Texture2D>("Textures/Player/PlayerMoveLeft")
            };

            gameInstance.Player = new GameSprite(Content, Vector3.Zero, true,1.5f,Tes,true, 1f, 0f);
            gameInstance.Player.isPlayer = true;

            gameInstance.Objects = new List<GameObject>();
            gameInstance.ObjectsDecor = new List<GameObject>();
            gameInstance.ObjectsDynamic = new List<GameObject>();
            gameInstance.DecorHitbox = new List<BoundingBox>();


            Build();
            gameInstance.ObjectsDynamic.Add(gameInstance.Player);
            foreach (GameObject  obj in gameInstance.ObjectsDynamic)
            {
                gameInstance.Objects.Add(obj);
            }
            foreach (GameObject obj in gameInstance.ObjectsDecor)
            {
                gameInstance.Objects.Add(obj);
            }

            gameInstance.Camera = new Camera(Game1.graphics.GraphicsDevice, gameInstance.Player);
            gameInstance.Hammer = new Hammer(Content, gameInstance.Player.Position);
        }

        protected void BuildWall(Vector3 positionMin, Vector3 positionMax, Color color = default)
        {
            Tuple<List<GameObject>, BoundingBox> Wall;

            if (color == default) color = Color.Gray;

            Wall = Managers.DecorConstruct.CreateWall(Content, positionMin, positionMax, color);

            gameInstance.ObjectsDecor.Add(new DecorObject(gameInstance.Content, Wall.Item2, Wall.Item1, Wall.Item1[0].Elasticity));
        }

        protected List<GameObject> BuildDoor(Vector3 positionMin, Vector3 positionMax)
        {
            List<GameObject> bigDoor;

            bigDoor = Managers.DecorConstruct.CreateBigDoor(Content, positionMin, positionMax);
            foreach (Door door in bigDoor)
            {
                gameInstance.ObjectsDynamic.Add(door);
            }

            return bigDoor;
        }

        protected void BuildPressurePlate(Vector3 positionMin, Vector3 positionMax, List<GameObject> bigDoor)
        {
            List<GameObject> pressurePlate;

            pressurePlate = Managers.DecorConstruct.CreatePressurePlate(Content, positionMin, positionMax, bigDoor);
            foreach (PressurePlate plate in pressurePlate)
            {
                gameInstance.ObjectsDecor.Add(plate);
            }
        }
    }
}
