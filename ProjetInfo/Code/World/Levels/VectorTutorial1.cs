﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ProjetInfo.Code.Objects;
using System.Collections.Generic;


namespace ProjetInfo.Code.World.Levels
{
    class VectorTutorial1 : Level
    {
        public VectorTutorial1(Game1 game) : base(game)
        {
            nextLevel = new Stage1(game);
            Text = new List<string> { "Appuyez sur RB pour activer et desactiver la slow mo.", "En slow mo, utilisez le stick gauche pour modifier les vitesses des objets." };
        }

        protected override void Build()
        {
            List<GameObject> bigDoor;
            gameInstance.Player.Position = new Vector3(5, 2, 5);

            //Wall
            BuildWall(new Vector3(39, 0, -1), new Vector3(40, 3, 41));

            //sol
            BuildWall(new Vector3(0, -1, 0), new Vector3(60, 0, 40));
            BuildWall(new Vector3(60, -1, 20), new Vector3(70, 0, 40), Color.Green);
            //bordure
            BuildWall(new Vector3(0, -1, 0), new Vector3(70, 1, -1));
            BuildWall(new Vector3(0, -1, 40), new Vector3(60, 1, 41));
            BuildWall(new Vector3(0, -1, -1), new Vector3(-1, 1, 41));
            BuildWall(new Vector3(70, -1, -1), new Vector3(71, 1, 20));
            //Door
            bigDoor = BuildDoor(new Vector3(60, 0, 20), new Vector3(61, 5, 39));
            //PressurePlate
            BuildPressurePlate(new Vector3(60, -2, 0), new Vector3(70, -1, 20), bigDoor);
            //building
            BuildWall(new Vector3(60, 0, 20), new Vector3(70, 5, 21));
            BuildWall(new Vector3(60, 0, 39), new Vector3(70, 5, 40));
            BuildWall(new Vector3(70, 0, 20), new Vector3(71, 5, 40));
            BuildWall(new Vector3(60, 5, 20), new Vector3(70, 6, 40));
            //ball
            gameInstance.ObjectsDynamic.Add(new BallObject(gameInstance.Content, new Vector3(20, 3, 20), true, 1f, true, 0.10f, 0.1f, "Textures/TextureGranite"));
        }
        public override bool UpdateLevel()
        {
            Vector3 pos = gameInstance.Player.Position;
            bool win = 61 < pos.X && pos.X < 69 && pos.Y < 5 && 21 < pos.Z && pos.Z < 39;

            if (win)
            {
                gameInstance.CurrentLevel = nextLevel;
                gameInstance.CurrentLevel.Load();
            }

            return win;
        }
    }
}
