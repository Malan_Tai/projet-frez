﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using ProjetInfo.Code.Objects;
using Microsoft.Xna.Framework.Graphics;

namespace ProjetInfo.Code.World.Levels
{
    class MovementTutorial : Level
    {
        public MovementTutorial(Game1 game) : base(game)
        {
            nextLevel = new HammerTutorial(game);
            Text = new List<string> { "Appuyez sur A pour sauter.", "Maintenez A pour sauter plus haut.", "Appuyez sur B pour dash." };
        }

        protected override void Build()
        {

            BuildWall(new Vector3(0, 0, 0), new Vector3(7, 4, 1));
            BuildWall(new Vector3(0, 0, 7), new Vector3(8, 4, 8));
            BuildWall(new Vector3(0, 0, 0), new Vector3(1, 4, 8));
            BuildWall(new Vector3(0, 3, 0), new Vector3(7, 4, 8));

            BuildWall(new Vector3(7, 0, 0), new Vector3(15, 14, 1));
            BuildWall(new Vector3(7, 0, 7), new Vector3(15, 14, 8));
            BuildWall(new Vector3(7, 4, 0), new Vector3(8, 14, 8));
            BuildWall(new Vector3(7, 13, 0), new Vector3(15, 14, 8));

            BuildWall(new Vector3(15, 0, 0), new Vector3(23, 23, 1));
            BuildWall(new Vector3(15, 0, 7), new Vector3(23, 23, 8));
            BuildWall(new Vector3(15, 14, 0), new Vector3(16, 23, 8));
            BuildWall(new Vector3(15, 22, 0), new Vector3(23, 23, 8));

            BuildWall(new Vector3(22, 0, 0), new Vector3(23, 23, 8));

            BuildWall(new Vector3(40, 0, 0), new Vector3(48, 23, 1));
            BuildWall(new Vector3(40, 0, 7), new Vector3(48, 23, 8));
            BuildWall(new Vector3(40, 0, 0), new Vector3(41, 19, 8));
            BuildWall(new Vector3(40, 18, 0), new Vector3(48, 19, 8));
            

            BuildWall(new Vector3(47, 0, 0), new Vector3(48, 23, 8));

            BuildWall(new Vector3(47, 22, 0), new Vector3(54, 23, 8), Color.Green);

            gameInstance.Player.Position = new Vector3(-10, 0, 4);
        }
    
        public override bool UpdateLevel()
        {
            Vector3 pos = gameInstance.Player.Position;
            bool win = 47 < pos.X && pos.X < 54 && pos.Y > 23 && 0 < pos.Z && pos.Z < 8;

            if (win)
            {
                gameInstance.CurrentLevel = nextLevel;
                gameInstance.CurrentLevel.Load();
            }

            return win;
        }
    }
}
