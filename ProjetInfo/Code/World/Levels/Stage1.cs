﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ProjetInfo.Code.Objects;
using System.Collections.Generic;


namespace ProjetInfo.Code.World.Levels
{
    class Stage1 : Level
    {
        public Stage1(Game1 game) : base(game)
        {
        }

        protected override void Build()
        {
            
            //main platform
            BuildWall(new Vector3(0, 14, 0), new Vector3(45, 15, 15));
            //win zone
            BuildWall(new Vector3(45, 14, 0), new Vector3(56, 15, 15), Color.Green);
            BuildWall(new Vector3(45, 15, 0), new Vector3(56, 21, 1));
            BuildWall(new Vector3(55, 15, 1), new Vector3(56, 21, 15));
            BuildWall(new Vector3(45, 15, 14), new Vector3(56, 21, 15));
            BuildWall(new Vector3(45, 20, 0), new Vector3(56, 21, 15));
            List<GameObject> endDoor = BuildDoor(new Vector3(45, 15, 0), new Vector3(46, 20, 14));

            //building
            BuildWall(new Vector3(30, 30, 30), new Vector3(31, 40, 45));
            BuildWall(new Vector3(44, 30, 30), new Vector3(45, 40, 45));
            BuildWall(new Vector3(30, 39, 30), new Vector3(45, 40, 45));
            BuildWall(new Vector3(30, 30, 44), new Vector3(45, 40, 45));
            BuildPressurePlate(new Vector3(31, 29, 31), new Vector3(44, 30, 44), endDoor);
            List<GameObject> midDoor = BuildDoor(new Vector3(30, 30, 30), new Vector3(44, 39, 31));

            //bloc
            BuildWall(new Vector3(15, 15, 0), new Vector3(30, 30, 1));
            BuildWall(new Vector3(15, 15, -15), new Vector3(30, 30, -14));
            BuildWall(new Vector3(15, 15, -30), new Vector3(30, 30, -29));
            BuildWall(new Vector3(15, 15, -15), new Vector3(16, 30, 0));
            BuildWall(new Vector3(29, 15, -15), new Vector3(30, 30, 0));
            BuildWall(new Vector3(15, 14, -30), new Vector3(30, 15, 0));
            BuildWall(new Vector3(15, 29, -15), new Vector3(30, 30, 0));

            BuildWall(new Vector3(0, 15, 0), new Vector3(15, 17, 1));
            BuildWall(new Vector3(0, 19, 0), new Vector3(15, 30, 1));
            BuildWall(new Vector3(0, 14, -15), new Vector3(1, 30, 1));
            BuildPressurePlate(new Vector3(1, 13, -15), new Vector3(15, 14, 0), midDoor);


            gameInstance.ObjectsDynamic.Add(new BallSpawner(Content, new Vector3(37, 60, 22), 3f, gameInstance));
            gameInstance.ObjectsDynamic.Add(new BallSpawner(Content, new Vector3(22, 40, -23), -1f, gameInstance));

            gameInstance.Player.Position = new Vector3(7, 17, 7);
        }

        public override bool UpdateLevel()
        {
            foreach (GameObject obj in gameInstance.ObjectsDynamic)
            {
                if (obj.Position.Y < 2)
                {
                    Console.Out.WriteLine("plouf");
                    if (obj == gameInstance.Player) Load();
                    else gameInstance.ObjectsToRemove.Add(obj);
                }
            }

            Vector3 pos = gameInstance.Player.Position;
            bool win = false;

            if (win)
            {
                gameInstance.CurrentLevel = nextLevel;
                gameInstance.CurrentLevel.Load();
            }

            return win;
        }
    }
}

