﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ProjetInfo.Code.Managers;
using System;
using System.Collections.Generic;


namespace ProjetInfo.Code.Objects
{
    public class BallObject : GameObject
    {
        public override bool DecorCollide { get; set; }
        override public BoundingBox Hitbox { get; set; }
        private float Radius { get; set; }

        public BallObject(ContentManager content, Vector3 pos, bool collisionsOn, float radius, bool gravityEffect = true, float m = 1f, float e = 0.9f, string texture = "false", string model = "Ball")
            : base(content, pos, collisionsOn, gravityEffect, m, e, texture, model)
        {
            DecorCollide = true;
            Radius = radius;
            GetHitboxs();
        }

        override public void Update(GameTime gameTime, float gameSpeed, List<GameObject> gameObjects)
        {
            base.Update(gameTime,gameSpeed, gameObjects);

            Matrix translation = Matrix.CreateTranslation(Position);
            float phi =- Radius * (float)CoordinatesManager.Norme(new Vector3(Speed.X, 0, Speed.Z));
            float theta = MathHelper.ToRadians(CoordinatesManager.CartesianToSpheric(Speed).Y);
            Matrix rotation = Matrix.CreateRotationX(phi)  * Matrix.CreateRotationY(theta) * Matrix.CreateScale(Radius);

            WorldMatrix = rotation * translation ;

        }
        override public BoundingBox GetHitboxs()
        {
            Hitbox = new BoundingBox(new Vector3(-Radius*2, -Radius*2, -Radius*2) + Position, new Vector3(Radius*2, Radius*2, Radius*2) + Position);
            return Hitbox;
        }

    }
}
