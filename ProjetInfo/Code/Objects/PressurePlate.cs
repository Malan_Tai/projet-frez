﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ProjetInfo;
using ProjetInfo.Code.Managers;

namespace ProjetInfo.Code.Objects
{
    class PressurePlate : GameObject
    {
        override public BoundingBox Hitbox { get; set; }
        override public bool DecorCollide { get; set; }

        public List<GameObject> target;
        public bool curentFrameActivated = false;

        public PressurePlate(ContentManager content, Vector3 pos, List<GameObject> targetedDoor, string model = "cube")
                        : base(content, pos, true, false, texture: "Textures/TextureGranite")
        {
            DecorCollide = false;
            Model = content.Load<Model>(model);
            WorldMatrix = Matrix.CreateScale(1) * Matrix.CreateTranslation(Position);
            target = targetedDoor;
            GetHitboxs();
        }

        public void IsStepedOn()
        {
            curentFrameActivated = true;
        }

        public override void Update(GameTime gameTime, float gameSpeed, List<GameObject> gameObjects)
        {
            if (curentFrameActivated)
            {
                foreach (Door door in target)
                {
                    door.OpenDoor();
                }
            }
            curentFrameActivated = false;

            prevAngle = Angle;
            Matrix translation = Matrix.CreateTranslation(Position);
            WorldMatrix = translation;
        }

        override public BoundingBox GetHitboxs()
        {
            Hitbox = new BoundingBox(new Vector3(-1, -1, -1) + Position, new Vector3(1, 1, 1) + Position);
            return Hitbox;
        }
    }
}
