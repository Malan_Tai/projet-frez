﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace ProjetInfo.Code.Objects
{
    class BallSpawner : GameObject
    {
        override public BoundingBox Hitbox { get; set; }
        override public bool DecorCollide { get; set; }
        override public BoundingBox GetHitboxs() { return new BoundingBox(new Vector3(0, 0, 0), new Vector3(0, 0, 0)); }

        private float spawnTimer;
        private float spawnMaxTime;
        private GameObject linkedBall;
        private bool linked;

        private Game1 gameInstance;

        public BallSpawner(ContentManager content, Vector3 pos, float spawnTime, Game1 game)
        : base(content, pos, false, m: 0, e: 0)
        {
            DecorCollide = false;
            spawnTimer = 0f;
            spawnMaxTime = spawnTime;
            gameInstance = game;

            linked = spawnTime < 0f;
        }

        public override void Update(GameTime gameTime, float gameSpeed, List<GameObject> gameObjects)
        {
            if (!linked)
            {
                spawnTimer += (float)gameTime.ElapsedGameTime.TotalSeconds * gameSpeed;

                if (spawnTimer >= spawnMaxTime)
                {
                    spawnTimer = 0f;
                    GameObject obj = new BallObject(Content, Position, true, 1f, true, 0.10f, 0.1f, "Textures/TextureGranite");
                    gameInstance.ObjectsToAdd.Add(obj);
                    Console.Out.WriteLine("pop");
                }
            }
            else if (linkedBall == null || !gameInstance.ObjectsDynamic.Contains(linkedBall))
            {
                GameObject obj = new BallObject(Content, Position, true, 1f, true, 0.10f, 0.1f, "Textures/TextureGranite");
                linkedBall = obj;
                gameInstance.ObjectsToAdd.Add(obj);
                Console.Out.WriteLine("pop");
            }
        }
    }
}
