﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ProjetInfo;
using ProjetInfo.Code.Managers;

namespace ProjetInfo.Code.Objects
{
    class Door : GameObject
    {
        override public BoundingBox Hitbox { get; set; }
        override public bool DecorCollide { get; set; }

        public Vector3 posInit;
        public Vector3 posFinal;
        public string state = "closing";
        public float doorSpeed = 10f;

        public Door(ContentManager content, Vector3 posStart, Vector3 posEnd, string model = "cube")
                        : base(content, posStart, true, false, texture: "Textures/TextureGranite")
        {
            DecorCollide = false;
            Model = content.Load<Model>(model);
            posInit = posStart;
            posFinal = posEnd;
            GetHitboxs();
            WorldMatrix = Matrix.CreateScale(1);
        }

        public bool OpenDoor()
        {
            if (Position != posFinal)
            {
                state = "opening";
            }
            return (Position == posFinal);
        }


        public override void Update(GameTime gameTime, float gameSpeed, List<GameObject> gameObjects)
        {
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds * gameSpeed;
            if (state == "opening")
            {
                Vector3 dir = (posFinal - posInit);
                dir.Normalize();
                Position +=  dir * doorSpeed * deltaTime;
                if(Vector3.Dot(dir,(posFinal-Position)) <= 0)
                {
                    Position = posFinal;
                }
            }
            else if (state == "closing")
            {
                Vector3 dir = (posInit - posFinal);
                dir.Normalize();
                Position += dir * doorSpeed * deltaTime;
                if (Vector3.Dot(dir, (posInit - Position)) <= 0)
                {
                    Position = posInit;
                }
            }

            state = "closing";
            GetHitboxs();

            prevAngle = Angle;
            Matrix translation = Matrix.CreateTranslation(Position);
            WorldMatrix = translation;
        }
        override public BoundingBox GetHitboxs()
        {
            Hitbox = new BoundingBox(new Vector3(-1, -1, -1) + Position, new Vector3(1, 1, 1) + Position);
            return Hitbox;
        }
    }
}
