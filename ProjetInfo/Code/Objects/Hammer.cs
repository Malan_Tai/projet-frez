using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ProjetInfo;
using ProjetInfo.Code.Managers;
using ProjetInfo.Code.Objects;

namespace ProjetInfo.Code.Objects
{
    public class Hammer
    {
        public Vector3 HeadPosition { get; set; }
        public Vector3 KnobPosition { get; set; }
        public Vector3 HammerDirectionVector;
        public Vector3 HammerSpeed;
        public Vector3 prevHeadPosition { get; set; }
        public Vector3 prevDirectionVector;

        public BoundingBox largeCollisionbox;
        public float[] largeCollisionboxCorners = { -1f, -1f, -1f, 1f, 1f, 1f };
        public Vector3 barycentre;


        public bool collide;
        public int collisionLevel;
        public bool isAffectedByGravity = false;

        public bool hammerHeld;
        public bool hammerSwing;
        public float chargeLevel;
        public bool rotaDone = false;

        public static float hammerRotationSpeed = 10000f; //vitesse de rotation du marteau
        public static float hammerSwingSpeed = 250f;
        public static float preSwingAngle;
        public static float postSwingAngle;
        public static float phi = 0f;               //angle vertical, dans [0, 180[
        public static float theta = 0f;             //angle horizontal, dans [0, 360[

        public static GamePadState GamePadState;
        public static GamePadState PreviousGamePadState;

        public static bool hammerInUse = false;
        private static int hammerPhase = 0;

        public bool InUse { get; set; }

        public Model Model;

        public Matrix WorldMatrix
        {
            get
            {
                Matrix pos = Matrix.CreateTranslation(KnobPosition - CoordinatesManager.SphericToCartesian(HammerDirectionVector));
                Matrix rotX = Matrix.CreateRotationX(MathHelper.ToRadians(HammerDirectionVector.Z + 180));
                Matrix rotY = Matrix.CreateRotationY(MathHelper.ToRadians(HammerDirectionVector.Y));
                Matrix scale = Matrix.CreateScale(1 / 15f);

                return scale * rotX * rotY * pos;
            }
        }

        public Hammer(ContentManager content, Vector3 knobPos, string hammerModel = "hammer")
        {
            Vector3 hammer = new Vector3(0, 4, 0);
            KnobPosition = knobPos;
            HeadPosition = knobPos - hammer;
            prevHeadPosition = HeadPosition;
            HammerSpeed = Vector3.Zero;

            hammerHeld = false;
            collisionLevel = 0;
            chargeLevel = -1f;

            HammerDirectionVector = CoordinatesManager.CartesianToSpheric(hammer);
            prevDirectionVector = HammerDirectionVector;

            Model = content.Load<Model>(hammerModel);
            collide = false;
            InUse = false;


            largeCollisionbox = GetLargeCollisionBox(HeadPosition, new Vector3(0, 0, 0));
        }

        virtual public void Draw(Camera camera)
        {
            if (InUse)
            {
                Matrix[] modelTransforms = new Matrix[Model.Bones.Count];
                Model.CopyAbsoluteBoneTransformsTo(modelTransforms);
                foreach (var mesh in Model.Meshes)
                {
                    foreach (BasicEffect effect in mesh.Effects)
                    {
                        effect.EnableDefaultLighting();
                        effect.PreferPerPixelLighting = true;

                        effect.World = modelTransforms[mesh.ParentBone.Index] * WorldMatrix;
                        effect.View = camera.ViewMatrix;
                        effect.Projection = camera.ProjectionMatrix;
                    }
                    mesh.Draw();
                }
            }
        }

        public void HammerBlow(Game1 game, GameTime gameTime, float gameSpeed, List<GameObject> gameObjects)
        {
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds * gameSpeed;

            foreach (GameObject obj in gameObjects)
            {
                if (obj.collide)
                {
                    if (ProcessCollision(obj.Hitbox, new Vector3(HammerSpeed.X, 0, 0) * deltaTime))
                    {
                        if (ProcessCollisionFine(obj.Model, new Vector3(HammerSpeed.X, 0, 0) * deltaTime))
                        {
                            Vector3 forceDirectionVector = (obj.Position + obj.barycentre) - game.Player.Position;
                            forceDirectionVector.Normalize();
                            forceDirectionVector = forceDirectionVector * 450;
                            obj.Forces.Add(forceDirectionVector);
                        }

                    }
                    else if (ProcessCollision(obj.Hitbox, new Vector3(0, HammerSpeed.Y, 0) * deltaTime))
                    {
                        Console.Out.WriteLine("Touch Y");
                        if (ProcessCollisionFine(obj.Model, new Vector3(0, HammerSpeed.Y, 0) * deltaTime))
                        {
                            Console.Out.WriteLine("Touch Y.2");
                            Vector3 forceDirectionVector = (obj.Position + obj.barycentre) - game.Player.Position;
                            forceDirectionVector.Normalize();
                            forceDirectionVector = forceDirectionVector * 450;
                            obj.Forces.Add(forceDirectionVector);
                        }

                    }
                    else if (ProcessCollision(obj.Hitbox, new Vector3(0, 0, HammerSpeed.Z) * deltaTime))
                    {
                        Console.Out.WriteLine("Touch Z");
                        if (ProcessCollisionFine(obj.Model, new Vector3(0, 0, HammerSpeed.Z) * deltaTime))
                        {
                            Console.Out.WriteLine("Touch Z.2");
                            Vector3 forceDirectionVector = (obj.Position + obj.barycentre) - game.Player.Position;
                            forceDirectionVector.Normalize();
                            forceDirectionVector = forceDirectionVector * 450;
                            obj.Forces.Add(forceDirectionVector);
                        }

                    }

                }
            }
        }

        public BoundingBox GetLargeCollisionBox(Vector3 pos, Vector3 offset)
        {
            BoundingBox box = new BoundingBox(new Vector3(pos.X + largeCollisionboxCorners[0] + offset.X, pos.Y + largeCollisionboxCorners[1] + offset.Y, pos.Z + largeCollisionboxCorners[2] + offset.Z), new Vector3(pos.X + largeCollisionboxCorners[3] + offset.X, pos.Y + largeCollisionboxCorners[4] + offset.Y, pos.Z + largeCollisionboxCorners[5] + offset.Z));
            return box;
        }

        public bool ProcessCollision(BoundingBox otherBox, Vector3 offset)
        {
            largeCollisionbox = GetLargeCollisionBox(HeadPosition, offset);
            if (largeCollisionbox.Intersects(otherBox) || largeCollisionbox.Contains(otherBox) != ContainmentType.Disjoint || otherBox.Contains(largeCollisionbox) != ContainmentType.Disjoint)
            {
                return true;
            }
            return false;
        }

        public bool ProcessCollisionFine(Model otherObject, Vector3 offset)
        {
            HeadPosition += offset;
            bool collisionDetected = false;
            foreach (ModelMesh targetMeshes in otherObject.Meshes)
            {
                foreach (ModelMesh ownerMeshes in Model.Meshes)
                {
                    if (targetMeshes.BoundingSphere.Intersects(ownerMeshes.BoundingSphere))
                    {
                        collisionDetected = true;
                    }
                }
            }

            HeadPosition -= offset;
            return collisionDetected;

        }

        public void DrawHitBox()
        {
            Vector3[] CornersList = new Vector3[8];

            CornersList[0] = HeadPosition + new Vector3(largeCollisionboxCorners[0], largeCollisionboxCorners[1], largeCollisionboxCorners[2]);
            CornersList[1] = HeadPosition + new Vector3(largeCollisionboxCorners[0], largeCollisionboxCorners[1], largeCollisionboxCorners[5]);
            CornersList[2] = HeadPosition + new Vector3(largeCollisionboxCorners[0], largeCollisionboxCorners[4], largeCollisionboxCorners[2]);
            CornersList[3] = HeadPosition + new Vector3(largeCollisionboxCorners[0], largeCollisionboxCorners[4], largeCollisionboxCorners[5]);
            CornersList[4] = HeadPosition + new Vector3(largeCollisionboxCorners[3], largeCollisionboxCorners[1], largeCollisionboxCorners[2]);
            CornersList[5] = HeadPosition + new Vector3(largeCollisionboxCorners[3], largeCollisionboxCorners[1], largeCollisionboxCorners[5]);
            CornersList[6] = HeadPosition + new Vector3(largeCollisionboxCorners[3], largeCollisionboxCorners[4], largeCollisionboxCorners[2]);
            CornersList[7] = HeadPosition + new Vector3(largeCollisionboxCorners[3], largeCollisionboxCorners[4], largeCollisionboxCorners[5]);

            foreach (Vector3 start in CornersList)
            {
                foreach (Vector3 end in CornersList)
                {
                    Game1.DrawLine(start, end);
                }
            }
        }

        public void GetInput(Game1 game, GameTime gameTime, float realDeltaTime, float deltaTime, GamePadState GamePadState, GamePadState PreviousGamePadState)
        {
            KnobPosition = game.Player.Position;
            HeadPosition = -CoordinatesManager.SphericToCartesian(HammerDirectionVector) + KnobPosition;
            if (PreviousGamePadState.Buttons.X == ButtonState.Released && GamePadState.Buttons.X == ButtonState.Pressed && (!hammerInUse))
            {
                KnobPosition = game.Player.Position + new Vector3(0, 1f, 0);
                HeadPosition = -CoordinatesManager.SphericToCartesian(HammerDirectionVector) + KnobPosition;
                hammerInUse = true;
                HammerDirectionVector.Y = theta + 90;
                HammerDirectionVector.Z = 90;
                preSwingAngle = HammerDirectionVector.Y;
            }

            if (hammerInUse)
            {
                if (!rotaDone)
                {
                    if (PreviousGamePadState.Buttons.Y == ButtonState.Released && GamePadState.Buttons.Y == ButtonState.Pressed)
                    {
                        hammerInUse = false;
                    }
                    prevHeadPosition = HeadPosition;
                    prevDirectionVector = HammerDirectionVector;
                    HammerDirectionVector = CoordinatesManager.RotateSphericVector(HammerDirectionVector, hammerSwingSpeed * deltaTime, 0);
                    HeadPosition = -CoordinatesManager.SphericToCartesian(HammerDirectionVector) + KnobPosition;
                    HammerSpeed = HeadPosition - prevHeadPosition;
                    HammerBlow(game, gameTime, game.GameSpeed, game.ObjectsDynamic);

                    if (prevDirectionVector.Y < preSwingAngle && HammerDirectionVector.Y > preSwingAngle)
                    {
                        rotaDone = true;
                    }
                }

                if (rotaDone)
                {
                    collisionLevel = 0; // Le marteau ne collide avec rien
                    hammerInUse = false;
                    rotaDone = false;
                    //Remettre le marteau en position rangee
                }
            }
            InUse = hammerInUse;
        }

    }
}

