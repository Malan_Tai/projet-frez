﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ProjetInfo.Code.Managers;
using System;
using System.Collections.Generic;

namespace ProjetInfo.Code.Objects
{

    abstract public class GameObject
    {
        abstract public bool DecorCollide { get; set; }

        public Vector3 Position { get; set; }
        public Vector3 Speed { get; set; }
        public Vector3 Gravity { get; set; }
        public List<Vector3> Forces { get; set; }
        public ContentManager Content { get; set; }
        public float Mass { get; set; }
        public float Elasticity { get; set; }
        public float Friction = 0.01f;

        public BoundingBox largeCollisionbox;
        public float[] largeCollisionboxCorners = new float[6];
        public Vector3 barycentre;
        public bool collide = false;
        public bool isAffectedByGravity = true;
        public bool isTouchingGround = false;

        public bool isPlayer = false;
        public Texture2D ActualTexture;
        private Vector3 color;
        private bool isColored = false;
        private bool IsTextured = false;


        public float prevAngle;
        abstract public BoundingBox Hitbox { get; set; }
        public float Angle
        {
            get
            {
                if (Speed.Z == 0 && Speed.X == 0)
                {
                    return prevAngle;
                }
                else if (Speed.Z == 0)
                {
                    return MathHelper.PiOver2 * Math.Sign(Speed.X);
                }
                return (float)(Math.Atan(Speed.X / Speed.Z));
            }
        }

        public Matrix WorldMatrix { get; set; }
        private Matrix arroWorldMatrix;

        public Model Model { get; set; }


        private Model arrow;
        private Texture2D ArrowTexture;
        private bool showArrow;
        public BoundingBox box;

        abstract public BoundingBox GetHitboxs();


        public GameObject(ContentManager content, Vector3 pos, bool collisionsOn, bool gravityEffect = true, float m = 1f, float e = 0.9f, string texture = "false", string model = "Cube", Color color = default)
        {
            if (collide)
            {
                GetCenter();
            }

            Content = content;
            Position = pos;
            Speed = new Vector3(0, 0, 0);
            Gravity = new Vector3(0, Game1.GravityConstant, 0);
            Forces = new List<Vector3>();
            Mass = m;
            Elasticity = e;

            Model = content.Load<Model>(model);
            isAffectedByGravity = gravityEffect;
            if (texture != "false")
            {
                IsTextured = true;
                ActualTexture = Content.Load<Texture2D>(texture);
            }
            arrow = content.Load<Model>("arrow");
            ArrowTexture = content.Load<Texture2D>("Red");
            collide = collisionsOn;

            if (color != default)
            {
                this.color = new Vector3(color.R / 255f, color.G / 255f, color.B / 255f);
                isColored = true;
            }

        }
        virtual public void Update(GameTime gameTime, float gameSpeed, List<GameObject> gameObjects)
        {
            GroundFriction();


            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds * gameSpeed;
            isTouchingGround = Position.Y < 0.01f;

            if (isAffectedByGravity && !isTouchingGround)
            {
                Speed += Gravity * deltaTime * Mass;
            }
            foreach (Vector3 force in Forces)
            {
                Speed += force * deltaTime;
            }
            Forces.Clear();
            

            Move(gameObjects, Speed * deltaTime);

            prevAngle = Angle;
            Matrix translation = Matrix.CreateTranslation(Position);


            WorldMatrix = translation;
            showArrow = gameSpeed < 0.95f && Speed.Length() > 0.01f;
            if (showArrow)
            {
                Vector3 vect = CoordinatesManager.CartesianToSpheric(Speed);

                Matrix rotX = Matrix.CreateRotationX(MathHelper.ToRadians(vect.Z));
                Matrix rotY = Matrix.CreateRotationY(MathHelper.ToRadians(vect.Y));
                Matrix scale = Matrix.CreateScale(Speed.Length() / 30f);

                arroWorldMatrix = scale * rotX * rotY * translation;
            }
        }

        virtual public void Draw(GraphicsDeviceManager graphics, Camera camera)
        {
            Matrix[] modelTransforms = new Matrix[Model.Bones.Count];
            Model.CopyAbsoluteBoneTransformsTo(modelTransforms);
            foreach (var mesh in Model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.TextureEnabled = IsTextured;
                    effect.Texture = ActualTexture;
                    if (isColored)
                    {
                        effect.DiffuseColor = color;
                    }
                    effect.EnableDefaultLighting();
                    effect.PreferPerPixelLighting = true;

                    effect.World = modelTransforms[mesh.ParentBone.Index] * WorldMatrix;
                    effect.View = camera.ViewMatrix;
                    effect.Projection = camera.ProjectionMatrix;
                }
                mesh.Draw();
            }

            DrawArrow(graphics, camera);
        }

        virtual public void DrawArrow(GraphicsDeviceManager graphics, Camera camera)
        {
            if (showArrow)
            {
                foreach (var mesh in arrow.Meshes)
                {
                    foreach (BasicEffect effect in mesh.Effects)
                    {
                        effect.PreferPerPixelLighting = true;
                        effect.TextureEnabled = true;
                        effect.Texture = ArrowTexture;
                        effect.World = arroWorldMatrix;
                        effect.View = camera.ViewMatrix;
                        effect.Projection = camera.ProjectionMatrix;
                    }
                    mesh.Draw();
                }
            }

        }
        private void Move(List<GameObject> gameObjects, Vector3 offset)
        {
            Vector3 offsetTemp;
            Tuple<bool, GameObject> collision;
            
            if (Math.Abs(offset.X) > 1)
            {
                offsetTemp = new Vector3(0.5f * Math.Sign(offset.X), 0, 0);
                while (Math.Abs(offsetTemp.X) <= Math.Abs(offset.X))
                {
                    Position += new Vector3(0.5f * Math.Sign(offset.X), 0, 0);
                    collision = IsCollision(gameObjects);
                    if (collision.Item1)
                    {
                        Position -= new Vector3(0.5f * Math.Sign(offset.X), 0, 0);
                        Speed = new Vector3(-collision.Item2.Elasticity * Elasticity * Speed.X, Speed.Y, Speed.Z);
                        offsetTemp = offset;
                    }
                    offsetTemp += new Vector3(0.5f * Math.Sign(offset.X), 0, 0);
                }

            }
            else
            {
                Position += new Vector3(offset.X, 0, 0);
                collision = IsCollision(gameObjects);
                if (collision.Item1)
                {
                    Position -= new Vector3(offset.X, 0, 0);
                    Speed = new Vector3(-collision.Item2.Elasticity * Elasticity * Speed.X, Speed.Y, Speed.Z);
                }
            }

            if (Math.Abs(offset.Y) > 1)
            {
                offsetTemp = new Vector3(0, 0.5f * Math.Sign(offset.Y), 0);
                while (Math.Abs(offsetTemp.Y) <= Math.Abs(offset.Y))
                {
                    Position += new Vector3(0, 0.5f * Math.Sign(offset.Y), 0);
                    collision = IsCollision(gameObjects);
                    if (collision.Item1)
                    {
                        Position -= new Vector3(0, 0.5f * Math.Sign(offset.Y), 0);
                        Speed = new Vector3(Speed.X, -collision.Item2.Elasticity * Elasticity * Speed.Y, Speed.Z);
                        offsetTemp = offset;
                        if (Speed.Y < 0.1f)
                        {
                            isTouchingGround = true;

                                PressurePlate localPlate = collision.Item2 as PressurePlate;
                                if (localPlate != null && !(this is PressurePlate))
                                {
                                    localPlate.IsStepedOn();
                                }
                            
                            if (Math.Abs(Elasticity * collision.Item2.Elasticity * Speed.Y) > 5f)
                            {
                                Speed = new Vector3(Speed.X,- Elasticity * collision.Item2.Elasticity * Speed.Y, Speed.Z);
                            }
                            else
                            {
                                Speed = new Vector3(Speed.X, 0, Speed.Z);
                            }
                        }
                    }
                    offsetTemp += new Vector3(0, 0.5f * Math.Sign(offset.Y), 0);
                }
            }
            else
            {
                Position += new Vector3(0, offset.Y, 0);
                collision = IsCollision(gameObjects);
                if (collision.Item1)
                {
                    Position -= new Vector3(0, offset.Y, 0);
                    Speed = new Vector3(Speed.X, -collision.Item2.Elasticity * Elasticity * Speed.Y, Speed.Z);
                    if (Speed.Y < 0.5f)
                    {
                        isTouchingGround = true;

                        PressurePlate localPlate = collision.Item2 as PressurePlate;
                        if (localPlate != null && !(this is PressurePlate))
                        {
                            localPlate.IsStepedOn();
                        }

                        if (Math.Abs(Elasticity * collision.Item2.Elasticity * Speed.Y) > 5f)
                        {
                            Speed = new Vector3(Speed.X, -Elasticity * collision.Item2.Elasticity * Speed.Y, Speed.Z);
                        }
                        else
                        {
                            Speed = new Vector3(Speed.X, 0, Speed.Z);
                        }
                    }
                }
            }


            if (Math.Abs(offset.Z) > 1)
            {
                offsetTemp = new Vector3(0, 0, 0.5f * Math.Sign(offset.Z));
                while (Math.Abs(offsetTemp.Z) <= Math.Abs(offset.Z))
                {
                    Position += new Vector3(0, 0, 0.5f * Math.Sign(offset.Z));
                    collision = IsCollision(gameObjects);
                    if (collision.Item1)
                    {
                        Position -= new Vector3(0, 0, 0.5f * Math.Sign(offset.Z));
                        Speed = new Vector3(Speed.X, Speed.Y, -collision.Item2.Elasticity * Elasticity * Speed.Z);
                        offsetTemp = offset;
                    }
                    offsetTemp += new Vector3(0, 0, 0.5f * Math.Sign(offset.Z));
                }
            }
            else
            {
                Position += new Vector3(0, 0, offset.Z);
                collision = IsCollision(gameObjects);
                if (collision.Item1)
                {
                    Position -= new Vector3(0, 0, offset.Z);
                    Speed = new Vector3(Speed.X, Speed.Y, -collision.Item2.Elasticity * Elasticity * Speed.Z);
                }
            }




            if (Position.Y < 0 && isAffectedByGravity)
            {
                Speed = new Vector3(Speed.X, 0, Speed.Z);
                Position = new Vector3(Position.X, 0, Position.Z);
            }
        }
        public void RotateSpeedVector(Matrix rotation)
        {
            Speed = Vector3.Transform(Speed, rotation);
        }

        public Vector3 GetSpeed()
        {
            return (Speed);
        }

        public void SetSpeed(Vector3 localSpeed)
        {
            Speed = new Vector3(localSpeed.X, localSpeed.Y, localSpeed.Z);
        }

        public void DrawHitBox()
        {
            Vector3[] CornersList = new Vector3[8];

            CornersList[0] = Position + new Vector3(largeCollisionboxCorners[0], largeCollisionboxCorners[1], largeCollisionboxCorners[2]);
            CornersList[1] = Position + new Vector3(largeCollisionboxCorners[0], largeCollisionboxCorners[1], largeCollisionboxCorners[5]);
            CornersList[2] = Position + new Vector3(largeCollisionboxCorners[0], largeCollisionboxCorners[4], largeCollisionboxCorners[2]);
            CornersList[3] = Position + new Vector3(largeCollisionboxCorners[0], largeCollisionboxCorners[4], largeCollisionboxCorners[5]);
            CornersList[4] = Position + new Vector3(largeCollisionboxCorners[3], largeCollisionboxCorners[1], largeCollisionboxCorners[2]);
            CornersList[5] = Position + new Vector3(largeCollisionboxCorners[3], largeCollisionboxCorners[1], largeCollisionboxCorners[5]);
            CornersList[6] = Position + new Vector3(largeCollisionboxCorners[3], largeCollisionboxCorners[4], largeCollisionboxCorners[2]);
            CornersList[7] = Position + new Vector3(largeCollisionboxCorners[3], largeCollisionboxCorners[4], largeCollisionboxCorners[5]);

            foreach (Vector3 start in CornersList)
            {
                foreach (Vector3 end in CornersList)
                {
                    Game1.DrawLine(start, end);
                }
            }
        }

        public void GroundFriction()
        {
            if ((isTouchingGround && (Speed != Vector3.Zero)) && (!isPlayer))
            {
                Forces.Add(-0.3f * Speed);
            }
        }
        private Tuple<bool, GameObject> IsCollision(List<GameObject> objects)
        {
            GetHitboxs();
            foreach (GameObject obj in objects)
            {
                if (Hitbox.Intersects(obj.Hitbox))
                    return new Tuple<bool, GameObject>(true, obj); 
            }
            return new Tuple<bool, GameObject>(false, this);
        }
        private void GetCenter()
        {

            float xSum = 0;
            float ySum = 0;
            float zSum = 0;
            int count = 0;


            foreach (ModelMesh mMesh in Model.Meshes)
            {
                foreach (ModelMeshPart mmPart in mMesh.MeshParts)
                {

                    Vector3[] arrVectors = new Vector3[mmPart.NumVertices * 2];
                    mmPart.VertexBuffer.GetData<Vector3>(arrVectors);
                    foreach (Vector3 vertex in arrVectors)
                    {
                        xSum += vertex.X;
                        ySum += vertex.Y;
                        zSum += vertex.Z;
                        count += 1;

                    }

                }

            }

            barycentre = new Vector3(xSum / count, ySum / count, zSum / count);
        }
    }
}
