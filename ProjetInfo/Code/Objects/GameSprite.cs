﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ProjetInfo.Code.Managers;
using System;
using System.Collections.Generic;



namespace ProjetInfo.Code.Objects
{
    public class GameSprite : GameObject
    {
        override public BoundingBox Hitbox { get; set; }
        override public bool DecorCollide { get; set; }

        private float Height { get; set; }
        private float Width { get; set; }
        private float Scale = 0f;
        private VertexPositionTexture[] verticesTex { get; set; }
        private int[] indices { get; set; }
        private VertexBuffer vb { get; set; }
        private IndexBuffer ib { get; set; }

        BasicEffect effect;
        public List<Texture2D> Textures;
        /*indice du tableau :
         * 0 = static, de base
         * 1 = vers l'avant
         * 2 = vers l'arriére
         * 3 = vers la droite
         * 4 = vers la gauche */


        public GameSprite(ContentManager content, Vector3 pos, bool collisionsOn, float height, List<Texture2D> textures, bool gravityEffect = true, float m = 1f, float e = 0f)
                        : base(content, pos, collisionsOn, gravityEffect, m, e)
        {
            DecorCollide = true;
            Height = height*2;
            Textures = textures;
            ActualTexture = Textures[0];
            foreach (Texture2D tex in Textures)
            {
                if ((float)tex.Width / (float)tex.Height > Scale) Scale = (float)tex.Width / (float)tex.Height;
            }
            CreateIndexedObjectTextured();
            effect = new BasicEffect(Game1.graphics.GraphicsDevice);
            GetHitboxs();


        }

        override public void Draw(GraphicsDeviceManager graphics, Camera camera)
        {
            DrawArrow(graphics, camera);
            if (Textures.Count > 1)
            {

                Vector3 cameraForward = CoordinatesManager.Normalize(Position - camera.Position);
                Vector3 objectForward = CoordinatesManager.Normalize(Speed);
                Vector3 vectorRightCamera = CoordinatesManager.Normalize(Vector3.Cross(cameraForward, CoordinatesManager.VectorUp(cameraForward)));

                double scalar = Vector3.Dot(cameraForward, objectForward);
                double scalar2 = Vector3.Dot(vectorRightCamera, objectForward);

                if (Math.Abs(scalar) > Math.Abs(scalar2))
                {
                    if (scalar > 0)
                    {
                        OtherTexture(1);
                    }
                    else
                    {
                        OtherTexture(2);
                    }

                }
                else if (Math.Abs(Speed.X) > 0)
                {

                    if (scalar2 > 0)
                    {
                        OtherTexture(3);
                    }
                    else
                    {
                        OtherTexture(4);
                    }
                }
                else
                {
                    OtherTexture(0);
                }
            }

            WorldMatrix = Matrix.CreateConstrainedBillboard(Position, camera.Position, new Vector3(0, 1, 0), camera.Up, camera.Forward);


            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                graphics.GraphicsDevice.RasterizerState = RasterizerState.CullNone;
                graphics.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                graphics.GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
                effect.Texture = ActualTexture;
                effect.View = camera.ViewMatrix;
                effect.Projection = camera.ProjectionMatrix;
                effect.World = WorldMatrix;
                effect.VertexColorEnabled = false;
                effect.TextureEnabled = true;
                Game1.graphics.GraphicsDevice.DrawUserIndexedPrimitives(PrimitiveType.TriangleList, verticesTex, 0, verticesTex.Length, indices, 0, 2);
            }
        }

        public void OtherTexture(byte i)
        {
            ActualTexture = Textures[i];
        }
        override public BoundingBox GetHitboxs()
        {
            Vector3 positionMax = new Vector3(-0.5f, -Height / 2, -0.5f) + Position;
            Vector3 positionMin = new Vector3(0.5f, Height / 2, 0.5f) + Position;
            float t;
            if (positionMin.X > positionMax.X) { t = positionMin.X; positionMin.X = positionMax.X; positionMax.X = t; }
            if (positionMin.Y > positionMax.Y) { t = positionMin.Y; positionMin.Y = positionMax.Y; positionMax.Y = t; }
            if (positionMin.Z > positionMax.Z) { t = positionMin.Z; positionMin.Z = positionMax.Z; positionMax.Z = t; }
            Hitbox =  new BoundingBox(positionMin, positionMax);
            return Hitbox;
        }

        private void CreateIndexedObjectTextured()
        {
            Width = Height * Scale;
            
            verticesTex = new VertexPositionTexture[]
            {
                new VertexPositionTexture( new Vector3( Width/2, Height / 2 , 0)    ,new Vector2(1,0)),
                new VertexPositionTexture( new Vector3(Width/2 , -Height / 2 , 0)   ,new Vector2(1,1)),
                new VertexPositionTexture( new Vector3(-Width/2 , -Height / 2 , 0)  ,new Vector2(0,1)),
                new VertexPositionTexture( new Vector3(-Width/2 , Height / 2 , 0)   ,new Vector2(0,0)),
                new VertexPositionTexture( new Vector3(1 , 1 ,0)     ,new Vector2(1,0)),
                new VertexPositionTexture( new Vector3(-1 , 1 , 0)    ,new Vector2(1,1)),
                new VertexPositionTexture( new Vector3(-1 , -1 , 0)   ,new Vector2(0,1)),
                new VertexPositionTexture( new Vector3(1 , -1 , 0)    ,new Vector2(0,0)),
                new VertexPositionTexture( new Vector3(1 , 1 , 0)    ,new Vector2(1,0)),
                new VertexPositionTexture( new Vector3(1 , 1 , 0)     ,new Vector2(1,1)),
                new VertexPositionTexture( new Vector3(1 , -1 , 0)    ,new Vector2(0,1)),
                new VertexPositionTexture( new Vector3(1 , -1 , 0)   ,new Vector2(0,0)),
                new VertexPositionTexture( new Vector3(1 , -1 , 0)   ,new Vector2(1,0)),
                new VertexPositionTexture( new Vector3(1 , -1 , 0)    ,new Vector2(1,1)),
                new VertexPositionTexture( new Vector3(-1 , -1 , 0)   ,new Vector2(0,1)),
                new VertexPositionTexture( new Vector3(-1 , -1 , 0)  ,new Vector2(0,0)),
                new VertexPositionTexture( new Vector3(-1 , -1 , 0)  ,new Vector2(1,0)),
                new VertexPositionTexture( new Vector3(-1 , -1 , 0)   ,new Vector2(1,1)),
                new VertexPositionTexture( new Vector3(-1 , 1 , 0)    ,new Vector2(0,1)),
                new VertexPositionTexture( new Vector3(-1 , 1 , 0)   ,new Vector2(0,0)),
                new VertexPositionTexture( new Vector3(1 , 1 , 0)     ,new Vector2(1,0)),
                new VertexPositionTexture( new Vector3(1 , 1 , 0)    ,new Vector2(1,1)),
                new VertexPositionTexture( new Vector3(-1 , 1 , 0)   ,new Vector2(0,1)),
                new VertexPositionTexture( new Vector3(-1 , 1 , 0)    ,new Vector2(0,0))
            };

            indices = new int[]
            {
                0, 3, 2, 0, 2, 1,
                4, 7, 6, 4, 6, 5,
                8, 11, 10, 8, 10, 9,
                12, 15, 14, 12, 14, 13,
                16, 19, 18, 16, 18, 17,
                20, 23, 22, 20, 22, 21
            };

            vb = new VertexBuffer(Game1.graphics.GraphicsDevice, typeof(VertexPositionTexture), verticesTex.Length, BufferUsage.WriteOnly);
            vb.SetData(this.verticesTex);
            ib = new IndexBuffer(Game1.graphics.GraphicsDevice, IndexElementSize.ThirtyTwoBits, indices.Length, BufferUsage.WriteOnly);
            ib.SetData(this.indices);

            Game1.graphics.GraphicsDevice.SetVertexBuffer(this.vb);

        }
    }
}
