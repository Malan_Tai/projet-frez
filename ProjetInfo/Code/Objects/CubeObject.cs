﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace ProjetInfo.Code.Objects
{
    class CubeObject : GameObject
    {
        override public BoundingBox Hitbox { get; set; }
        override public bool DecorCollide { get; set; }

        override public BoundingBox GetHitboxs() { return new BoundingBox(new Vector3(0, 0, 0), new Vector3(0, 0, 0)); }

        public CubeObject(ContentManager content, Vector3 pos, bool collisionsOn = false, bool gravityEffect = false, float m = 1f, float e = 0.9f, string texture = "false", string model = "Cube", Color color = new Color())
        : base(content, pos, collisionsOn, gravityEffect, m, e, texture, model, color)
        {
            WorldMatrix = Matrix.CreateScale(0.5f) * Matrix.CreateTranslation(Position);
            DecorCollide = false;
        }

        public override void Update(GameTime gameTime, float gameSpeed, List<GameObject> gameObjects)
        {
        }
    }

}
