﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ProjetInfo.Code.Managers;

namespace ProjetInfo.Code.Objects
{
    public class Camera
    {
        public Vector3 Position { get; set; }
        public GameObject ObjectTarget { get; set; }

        public Vector3 Target { get; set; }

        // constants //
        private float fieldOfView = MathHelper.ToRadians(100f);
        private float nearClipPlane = 1;
        private float farClipPlane = 1000;
        private float aspectRatio;

        private Vector3 upVector = Vector3.UnitY;
        // constants //

        public Matrix ViewMatrix
        {
            get
            {
                return Matrix.CreateLookAt(Position, Target, upVector);
            }
        }
        public Matrix ProjectionMatrix
        {
            get
            {
                return Matrix.CreatePerspectiveFieldOfView(fieldOfView, aspectRatio, nearClipPlane, farClipPlane);
            }
        }
        public Vector3 Forward
        {
            get
            {
                Vector3 vect = Target - Position;
                vect.Normalize();
                return vect;
            }
        }
        public Vector3 Up
        {
            get
            {
                return CoordinatesManager.VectorUp(Forward);
            }
        }


        public Camera(GraphicsDevice graphics, GameObject target)
        {
            ObjectTarget = target;
            Target = target.Position;
            aspectRatio = graphics.Viewport.AspectRatio;
        }

        public void Update(GameTime gameTime)
        {
            Vector3 delta = ObjectTarget.Position - Target;

            Position += delta;
            Target += delta;
        }
    }
}
