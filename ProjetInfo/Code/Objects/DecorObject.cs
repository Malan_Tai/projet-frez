﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ProjetInfo.Code.Managers;
using System;
using System.Collections.Generic;

namespace ProjetInfo.Code.Objects
{
    class DecorObject : GameObject
    {
        override public BoundingBox Hitbox { get; set; }
        override public bool DecorCollide { get; set; }

        private List<GameObject> Objects { get; set; }

        public override BoundingBox GetHitboxs()
        {
            return Hitbox;
        }

        public DecorObject(ContentManager content, BoundingBox box, List<GameObject> objects,float e):base(content, Vector3.Zero, true, true, 1, e)
        {
            DecorCollide = true;
            Hitbox = box;
            Objects = objects;
        }
        public override void Update(GameTime gameTime, float gameSpeed, List<GameObject> gameObjects)
        {
        }

        public override void Draw(GraphicsDeviceManager graphics, Camera camera)
        {
            foreach(GameObject obj in Objects)
            {
                obj.Draw(graphics, camera);

            }
        }
    }
}
