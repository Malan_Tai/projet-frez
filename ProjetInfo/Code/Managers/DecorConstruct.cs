﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using ProjetInfo.Code.Objects;
using System;
using System.Collections.Generic;

namespace ProjetInfo.Code.Managers
{
    public class DecorConstruct
    {
        private static Vector3 offset = new Vector3(-.5f, -.5f, -.5f);
        public static Tuple<List<GameObject>, BoundingBox> CreateWall(ContentManager content, Vector3 positionMin, Vector3 positionMax, Color color)
        {
            List<GameObject> Wall = new List<GameObject> { };
            BoundingBox Hitbox;
            float t;
            if (positionMin.X > positionMax.X) { t = positionMin.X; positionMin.X = positionMax.X; positionMax.X = t; }
            if (positionMin.Y > positionMax.Y) { t = positionMin.Y; positionMin.Y = positionMax.Y; positionMax.Y = t; }
            if (positionMin.Z > positionMax.Z) { t = positionMin.Z; positionMin.Z = positionMax.Z; positionMax.Z = t; }

            Hitbox = new BoundingBox(positionMin + offset, positionMax + offset);
            for (float z = positionMin.Z; z < positionMax.Z; z++)
            {
                for (float y = positionMin.Y; y < positionMax.Y; y++)
                {
                    for (float x = positionMin.X; x < positionMax.X; x++)
                    {
                        Wall.Add(new CubeObject(content, new Vector3(x, y, z), color: color));

                    }
                }
            }
            return new Tuple<List<GameObject>, BoundingBox>(Wall, Hitbox);
        }
        public static List<GameObject> CreateBigDoor(ContentManager content, Vector3 positionMin, Vector3 positionMax)
        {
            int minX = (int)Math.Min(positionMin.X, positionMax.X);
            int maxX = (int)Math.Max(positionMin.X, positionMax.X);
            int minY = (int)Math.Min(positionMin.Y, positionMax.Y);
            int maxY = (int)Math.Max(positionMin.Y, positionMax.Y);
            int minZ = (int)Math.Min(positionMin.Z, positionMax.Z);
            int maxZ = (int)Math.Max(positionMin.Z, positionMax.Z);

            bool shortX;
            int mid;
            if (Math.Abs(positionMin.X - positionMax.X) <= 1)
            {
                shortX = true;
                mid = minZ + (int)(Math.Abs(positionMin.Z - positionMax.Z) / 2);
            }
            else
            {
                shortX = false;
                mid = minX + (int)(Math.Abs(positionMin.X - positionMax.X) / 2);
            }

            List<GameObject> bigDoor = new List<GameObject> { };

            for (float z = minZ; z < maxZ; z++)
            {
                for (float y = minY; y < maxY; y++)
                {
                    for (float x = minX; x < maxX; x++)
                    {
                        if (shortX)
                        {
                            if (z <= mid) bigDoor.Add(new Door(content, new Vector3(x, y, z), new Vector3(x, y, minZ)));
                            else bigDoor.Add(new Door(content, new Vector3(x, y, z), new Vector3(x, y, maxZ)));
                        }
                        else
                        {
                            if (x <= mid) bigDoor.Add(new Door(content, new Vector3(x, y, z), new Vector3(minX, y, z)));
                            else bigDoor.Add(new Door(content, new Vector3(x, y, z), new Vector3(maxX, y, z)));
                        }
                    }
                }
            }
            return bigDoor;
        }
        public static List<GameObject> CreatePressurePlate(ContentManager content, Vector3 positionMin, Vector3 positionMax, List<GameObject> bigDoor)
        {
            List<GameObject> pressurePlate = new List<GameObject> { };

            for (float z = Math.Min(positionMin.Z, positionMax.Z); z < Math.Max(positionMin.Z, positionMax.Z); z++)
            {
                for (float y = Math.Min(positionMin.Y, positionMax.Y); y < Math.Max(positionMin.Y, positionMax.Y); y++)
                {
                    for (float x = Math.Min(positionMin.X, positionMax.X); x < Math.Max(positionMin.X, positionMax.X); x++)
                    {
                        pressurePlate.Add(new PressurePlate(content, new Vector3(x, y, z), bigDoor));

                    }
                }
            }
            return pressurePlate;
        }
    }

}

