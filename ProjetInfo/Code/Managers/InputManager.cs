﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using ProjetInfo;
using ProjetInfo.Code;
using ProjetInfo.Code.Objects;

namespace ProjetInfo.Code.Managers
{
    static class InputManager
    {
        public static GamePadState GamePadState;
        public static GamePadState PreviousGamePadState;

        public static float phi = 0f;               //angle vertical, dans [0, 180[
        public static float theta = 0f;             //angle horizontal, dans [0, 360[
        public static float radius = 7f;            //distance entre la caméra et l'objet visé 
        public static float zoomSpeed = 10f;        //vitesse du zoom
        public static float rotationSpeed = 100f;   //vitesse de roation de la caméra 
        public static float moveSpeed = 20f;        //vitesse de déplacement du joueur
        public static float dashSpeed = 160f;       //vitesse de dash du joueur

        private static float leftStickControl = 1f;     //capacité du joueur à controler ses mouvements grace au stick gauche, dans [0, 1]
        private static float prevLeftSickControl = 1f;

        private static float dashTime = 0f;
        private static float maxDashTime = 0.3f;
        private static bool dashing = false;
        private static bool stoppedDashing = false;     //true si on a arrete de dasher cette frame

        

        public static void GetInput(Game1 game, GameTime gameTime)
        {
            PreviousGamePadState = GamePadState;
            GamePadState = GamePad.GetState(PlayerIndex.One, GamePadDeadZone.None);
            float realDeltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds * game.GameSpeed;

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape)) game.Exit();

            if (GamePadState.IsConnected)
            {
                prevLeftSickControl = leftStickControl;

                if (GamePadState.Buttons.Start == ButtonState.Pressed && PreviousGamePadState.Buttons.Start == ButtonState.Released)
                {
                    game.CurrentLevel.Load();
                }

                // CAMERA //
                float camX = GamePadState.ThumbSticks.Right.X;
                float camY = GamePadState.ThumbSticks.Right.Y;

                if (Math.Abs(camX) > 0.5)
                {
                    theta += rotationSpeed * camX * realDeltaTime;
                }
                if (Math.Abs(camY) > 0.5)
                {
                    phi -= rotationSpeed * camY * realDeltaTime;
                    if (phi < 1f) phi = 1f;
                    if (phi > 179f) phi = 179f;
                }

                // Zoom
                if (GamePadState.DPad.Up == ButtonState.Pressed)
                {
                    if (radius > 4)
                    {
                        radius -= zoomSpeed * realDeltaTime;
                    }
                }
                else if (GamePadState.DPad.Down == ButtonState.Pressed)
                {
                    if (radius < 15)
                    {
                        radius += zoomSpeed * realDeltaTime;
                    }
                }
                //

                game.Camera.Position = CoordinatesManager.SphericToCartesian(radius, theta, phi) + game.Camera.ObjectTarget.Position;
                // CAMERA //

                // SLOW MO //
                if (GamePadState.Buttons.RightShoulder == ButtonState.Pressed && PreviousGamePadState.Buttons.RightShoulder == ButtonState.Released)
                {
                    if (game.GameSpeed > 0.99f) game.GameSpeed = 0.001f;
                    else game.GameSpeed = 1f;
                }
                // SLOW MO //

                // MOVEMENT //
                stoppedDashing = false;
                if (dashing)
                {
                    dashTime += deltaTime;
                }
                if (dashTime >= maxDashTime)
                {
                    dashing = false;
                    stoppedDashing = true;
                    dashTime = 0f;
                    leftStickControl = 1f;
                }
                
                if (!game.Player.isTouchingGround)
                {
                    if (leftStickControl > 0.3f) leftStickControl = 0.3f;
                }
                else if (!dashing && !(game.Hammer.InUse))
                {
                    leftStickControl = 1f;
                }

                float prevDx = game.Player.Speed.X;
                float prevDz = game.Player.Speed.Z;

                float padX = GamePadState.ThumbSticks.Left.X;
                float padZ = -GamePadState.ThumbSticks.Left.Y;
                if (Math.Abs(padX) < 0.1) padX = 0;
                if (Math.Abs(padZ) < 0.1) padZ = 0;

                double thetaRad = MathHelper.ToRadians(theta);
                float dx = (float)(padX * Math.Cos(thetaRad) + padZ * Math.Sin(thetaRad)) * moveSpeed * leftStickControl;
                float dz = (float)(-padX * Math.Sin(thetaRad) + padZ * Math.Cos(thetaRad)) * moveSpeed * leftStickControl;
                // WIP //
                if (game.GameSpeed < 0.99f)
                {
                    float controlRatio;
                    if (prevLeftSickControl < 0.01f) controlRatio = leftStickControl;
                    else controlRatio = leftStickControl / prevLeftSickControl;

                    if (!stoppedDashing)
                    {
                        dx = prevDx;// * controlRatio;
                        dz = prevDz;// * controlRatio;
                    }
                    else
                    {
                        dx = prevDx * moveSpeed * controlRatio / dashSpeed;
                        dz = prevDz * moveSpeed * controlRatio / dashSpeed;
                    }
                }
                // WIP //
                else if (padX == 0 && padZ == 0 && !game.Player.isTouchingGround)
                {
                    dx = prevDx;
                    dz = prevDz;
                }
                float dy = game.Player.Speed.Y;


                // jump
                if (GamePadState.Buttons.A == ButtonState.Pressed && ((!game.Hammer.hammerHeld) && (!game.Hammer.hammerSwing)))
                {
                    
                    if (game.Player.isTouchingGround)
                    {
                        dy = 50f;
                        game.Player.Gravity = new Vector3(0, Game1.GravityConstant * 0.05f, 0);
                        game.Player.isTouchingGround = false;
                    }
                    else if (game.Player.Speed.Y > 0.1f)
                    {
                        game.Player.Gravity += new Vector3(0, -10, 0) * game.GameSpeed;
                    }
                    else
                    {
                        game.Player.Gravity = new Vector3(0, Game1.GravityConstant, 0);
                    }
                }
                else
                {
                    game.Player.Gravity = new Vector3(0, Game1.GravityConstant, 0);

                }


                //dash
                if (game.GameSpeed > 0.99f && GamePadState.Buttons.B == ButtonState.Pressed && PreviousGamePadState.Buttons.B == ButtonState.Released && !dashing
                    && !(game.Hammer.InUse))
                {
                    dx = (float)(padX * Math.Cos(thetaRad) + padZ * Math.Sin(thetaRad)) * dashSpeed;
                    dz = (float)(-padX * Math.Sin(thetaRad) + padZ * Math.Cos(thetaRad)) * dashSpeed;
                    leftStickControl = 0f;
                    dashing = true;
                }
                else if (dashing)
                {
                    dx = game.Player.Speed.X;
                    dz = game.Player.Speed.Z;
                }
                
                game.Player.Speed = new Vector3(dx, dy, dz);
                // MOVEMENT //


                // HAMMER //

                if (game.Hammer.InUse) { leftStickControl = 0.25f; }
                else { leftStickControl = 1f; }
                game.Hammer.GetInput(game, gameTime, realDeltaTime, deltaTime, GamePadState, PreviousGamePadState);

                // HAMMER //

                // VECTOR MODIFICATIONS
                if (game.GameSpeed < 0.99f)
                {
                    float leftCamX = GamePadState.ThumbSticks.Left.X;
                    float leftCamY = GamePadState.ThumbSticks.Left.Y;

                    
                    float dTheta = 0f;
                    float dPhi = 0f;

                    if (Math.Abs(leftCamX) > 0.5)
                    {
                        dTheta += rotationSpeed * leftCamX * realDeltaTime;
                    }
                    if (Math.Abs(leftCamY) > 0.5)
                    {
                        dPhi -= rotationSpeed * leftCamY * realDeltaTime;
                    }

                    if (dTheta != 0 || dPhi != 0)
                    {
                        foreach (GameObject obj in game.Objects)
                        {
                            Vector3 localSpeed = obj.GetSpeed();

                            if (localSpeed != new Vector3(0, 0, 0))
                            {
                                obj.SetSpeed(CoordinatesManager.RotateVector(localSpeed, dTheta, dPhi));
                            }
                        }
                    }
                }
            }

        }
    }
}
