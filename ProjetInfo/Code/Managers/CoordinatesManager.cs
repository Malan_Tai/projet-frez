﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ProjetInfo.Code.Objects;

namespace ProjetInfo.Code.Managers
{
    static class CoordinatesManager
    {
        public static Vector3 SphericToCartesian(float radius, float theta, float phi) //theta: horizontal dans [0, 360[ ; phi : vertical dans [0, 180[
        {
            float x;
            float y;
            float z;
            float thetaRad = MathHelper.ToRadians(theta);
            float phiRad = MathHelper.ToRadians(phi);

            x = (float)(radius * Math.Sin(phiRad) * Math.Sin(thetaRad));
            y = (float)(radius * Math.Cos(phiRad));
            z = (float)(radius * Math.Sin(phiRad) * Math.Cos(thetaRad));

            return new Vector3(x, y, z);
        }
        public static Vector3 SphericToCartesian(Vector3 spheric) //theta: horizontal dans [0, 360[ ; phi : vertical dans [0, 180[
        {
            float x;
            float y;
            float z;
            float radius = spheric.X;
            float thetaRad = MathHelper.ToRadians(spheric.Y);
            float phiRad = MathHelper.ToRadians(spheric.Z);

            x = (float)(radius * Math.Sin(phiRad) * Math.Sin(thetaRad));
            y = (float)(radius * Math.Cos(phiRad));
            z = (float)(radius * Math.Sin(phiRad) * Math.Cos(thetaRad));

            return new Vector3(x, y, z);
        }

        public static Vector3 RotateVector(Vector3 speed, float deltaTheta, float deltaPhi) // deltaTheta et deltaPhi en degrés
        {
            Vector3 sphericSpeed = CartesianToSpheric(speed);
            float radius = sphericSpeed.X;
            float theta = (sphericSpeed.Y + deltaTheta) % 360;
            float phi = (sphericSpeed.Z + deltaPhi) % 360;

            if (180f < phi)
            {
                theta = (theta + 180) % 360;
                phi = 360 - phi;
            }

            return SphericToCartesian(radius, theta, phi);

        }

        public static Vector3 CartesianToSpheric(Vector3 vector) // retourne phi et theta en degrés dans un vecteur3(rayon,theta,phi)
        {
            float radius;
            float thetaRad;
            float phiRad;
            float x = vector.X;
            float y = vector.Y;
            float z = vector.Z;

            radius = (float)Norme(vector);
            phiRad = (float)Math.Acos(y / radius);
            thetaRad = (float)Math.Atan2(x, z);

            return new Vector3(radius, MathHelper.ToDegrees(thetaRad), MathHelper.ToDegrees(phiRad));
        }

        public static Vector3 RotateSphericVector(Vector3 sphericVector, float deltaTheta, float deltaPhi) // deltaTheta et deltaPhi en degrés
        {
            float radius = sphericVector.X;
            float theta = (sphericVector.Y + deltaTheta) % 360;
            float phi = (sphericVector.Z + deltaPhi) % 360;

            if (180f <= phi)
            {
                theta = (theta + 180) % 360;
                phi = 360 - phi;
            }

            return new Vector3(radius, theta, phi);
        }

        public static Vector3 VectorUp(Vector3 vector)
        {
            Vector3 sphericalCoordinates = CartesianToSpheric(vector);

            float radius = sphericalCoordinates.X;
            float theta = sphericalCoordinates.Y ;
            float phi = sphericalCoordinates.Z + 90f;

            Vector3 cartesianCoordinates = SphericToCartesian(radius, theta, phi);
            cartesianCoordinates.Normalize();

            return cartesianCoordinates;
        }

        public static Matrix Rotation(Vector3 oldNormal, Vector3 newNormal)
        {
            float angle = (float)Math.Acos(Vector3.Dot(oldNormal, newNormal));
            Vector3 axis = Vector3.Cross(oldNormal, newNormal);
            axis.Normalize();

            return Matrix.CreateFromAxisAngle(axis, angle);
        }

        public static Vector3 Normalize(Vector3 vectorA)
        {
            vectorA.Normalize();
            return vectorA;
        }

        public static double Norme(Vector3 vectorA)
        {
            double norme = (float)Math.Sqrt(Math.Pow(vectorA.X, 2) + Math.Pow(vectorA.Y, 2) + Math.Pow(vectorA.Z, 2));

            return norme;
        }
    }
}
